 import java.util.Scanner;                                                                                     public class Application
{
	public static void main(String[]args)
	{
		// First student //

		Student student1 = new Student("Tony Do", 18);
		
		System.out.println(student1.getName() + ", " + student1.getAge() + ", " + student1.getGender());
		
		// Second student //
		
		Student student2 = new Student("Nson", 19);
		
		
		System.out.println(student2.getName() + ", " + student2.getAge() + ", " + student2.getGender());
		
		student1.sayAge();
		student2.sayAge();
		
		Student[] section3 = new Student[3];
		
		section3[0] = student1;
		section3[1] = student2;
		section3[2] = new Student("Emilia", 69);
		
		System.out.println("This is done with a get method " + section3[0].getName()); // .name was replaced by getName() but only first objects //
		System.out.println("This is done with a get method " + section3[1].getName()); // .name was replaced by getName() due to it being private //
		
		
		// Most of lab 4 is here, except some replacements had to be made above due to instructions //
		
		Scanner scan = new Scanner(System.in);
		
		int amountStudied = scan.nextInt(); // added a line to increment amountLearnt by an amount we choose //
		
		System.out.println(section3[0].getamountLearnt()); 
	
		section3[2].learn(amountStudied);  // calling learn twice //
		section3[2].learn(amountStudied);  // increments amountLearnt twice //
		
		System.out.println(section3[0].getamountLearnt()); // printing the values for each person, also replaced by a get method//
		
		System.out.println(section3[1].getamountLearnt());
	
		System.out.println(section3[2].getamountLearnt());
		
		section3[0].learn(5); // Calling learn but with a positive value //
		
		System.out.println("This is after calling learn for the first person " + section3[0].amountLearnt); // print result after calling learn
		
		// The results before calling learn is basically above before section3[0] //
		
		section3[1].learn(-2);
		
		System.out.println("This is after calling learn for the second person " + section3[1].amountLearnt);


		Student student3 = new Student("Emillia", 12);
		student3.setGender("Female");
	}
	
}