public class Student
{
	private String name;
	private String gender;
	private int age;
	int amountLearnt; // added stuff from Lab 03 //

	public Student(String name, int age)
	{
		this.name = name;
		this.age = age;
		this.gender = "Female";
		this.amountLearnt = 0;
	}

	public void sayAge()
	{
		System.out.println("I am " + age + " years old");
		
	}
	
	public void sayGender()
	{
		System.out.println("I am a " + gender);
		
	}
	
	public void learn(int amountStudied)
	{
		if (amountStudied > 0)
		{
		this.amountLearnt = this.amountLearnt + amountStudied;
		}
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getGender()
	{
		return this.gender;
	}
	
	public int getAge()
	{
		return this.age;
	}
	
	public int getamountLearnt()
	{
		return this.amountLearnt;
	}
	
	
	
	public void setGender(String newGender)
	{
		this.gender = newGender;
	}
	
	

}
	